# Home-Server

Configurations to have a basic server at home.

## Ports

List of the local ports used in a server:

- **81**: Nginx Proxy Manager Admin UI
- **88**: Nextcloud
- **8000**: Vaultwarden
- **9000**: Portainer HTTP
- **9443**: Portainer HTTPS

List of the exposed ports to the Internet:

- **80**: Reverse Proxy HTTP
- **443**: Reverse Proxy HTTPS
- **51820**: Wireguard VPN UDP

## Installation

First, boot from the `Ubuntu` ISO.

![GRUB](img/1.png)

Select the languaje.

![Languaje](img/2.png)

Select the keyboard layout.

![Keyboard](img/3.png)

In this case we only select the minimal installation.

![Minimal installation](img/4.png)

Let's start creating the partitions.

![Installation type](img/5.png)

First, create a new partition table.

![Partition table](img/6.png)

Create the necessary partitions:

![List of partitions](img/7.png)

Then, select your location.

![Berlin](img/8.png)

Configure the user and password. **Use other name in prod.**

![User config](img/9.png)

Wait until it's installed.

![Installing...](img/10.png)

Restart when It's completed.

![Restart](img/11.png)

Remove the installation device to boot from the disk.

![Remove USB](img/12.png)

## Configuration

Set the power settings to never turn the screen off:

![Power Settings](img/28.png)

Install additional languajes:

![Installing German Languaje](img/29.png)

Once It's installed, restart. The next time you login, select to **update the names of the folders**.

![Update the folders](img/30.png)

> Restart to apply the changes.

### Setup

Log in, and install `git` & `vim` to continue.

![Installing git and vim](img/13.png)

Then, clone this _repository_ on the server and navigate to the directory.

```bash
$ git clone https://gitlab.com/RSZT/home-server.git && cd home-server
```

Give executable **permissions** to all _bash scripts_ in this directory.

```bash
$ chmod +x *.sh
```

Now you can run the **setup** script.

```bash
$ ./setup.sh
```

### Fail2Ban

If we are going to expose the **SSH** port to _internet_, make sure to use **fail2ban**. Edit the `jail.conf` file like this:

```bash
$ vim /etc/fail2ban/jail.conf
```

Move to the `JAILS` section and edit the settings:

```python
#
# JAILS
#

#
# SSH servers
#

[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.

enabled = true
bantime = 86400 # 24 Hours
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
maxretry = 3
```

Then, **enable** the service and **start** the service.

```bash
$ sudo systemctl enable fail2ban

$ sudo systemctl start fail2ban

$ sudo systemctl status fail2ban
```

Now we can see the banned **IPs**:

```bash
cat /var/log/fail2ban.log
```

### Network

Configure the network to have a static **IP address**.

![Wired connection settings](img/14.png)

Check the new configuration in the _terminal_ using `ip a` and `ping` to **Google**.

![Check network settings](img/15.png)

Reboot the server to check everything is working.

```bash
$ sudo reboot now
```

### Disks

Format the **DATA** hard disk in _NTFS_.

![Format Volume](img/16.png)

Following this [tutorial](https://developerinsider.co/auto-mount-drive-in-ubuntu-server-22-04-at-startup/) we can learn to automount the external drive at **startup**. First, create the Mount Point.

```bash
root@utest:/media# mkdir DATA

root@utest:/media# lsblk -o NAME,FSTYPE,UUID,MOUNTPOINTS
```

Then, get the Drive **UUID** and **Type**.

![lsblk disks](img/17.png)

Now, edit the `/etc/fstab` file.

```bash
# DATA
UUID=6519C06610B80359 /media/DATA ntfs defaults 0 0
```

![fstab file](img/18.png)

**Test** `fstab` before rebooting!

```bash
$ sudo findmnt --verify
Success, no errors or warnings detected
```

![verify disks](img/19.png)

Reboot the server to check everything is working well.

```bash
$ sudo reboot now
```

Check the drive is mounted again.

```bash
$ lsblk -o NAME,FSTYPE,UUID,MOUNTPOINTS
```

## Services

### NextCloud

Edit the passwords and the other settings in the [.env](nextcloud/.env) file.

Start the containers:

```bash
$ docker compose up -d
```

#### Post-Configurations

Enter to the `App` container as **root**.

```bash
$ docker exec -it App bash
```

Update and install `vim`.

```bash
$ root@f7ecbe790da1:/var/www/html# apt update
Hit:1 http://deb.debian.org/debian bookworm InRelease
Hit:2 http://deb.debian.org/debian bookworm-updates InRelease
Hit:3 http://deb.debian.org/debian-security bookworm-security InRelease
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
1 package can be upgraded. Run 'apt list --upgradable' to see it.

$ root@f7ecbe790da1:/var/www/html# apt install vim
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
vim is already the newest version (2:9.0.1378-2).
0 upgraded, 0 newly installed, 0 to remove and 1 not upgraded.
```

Edit `config.php` and paste this string:

`'check_data_directory_permissions' => false,`

```bash
$ root@f7ecbe790da1:/var/www/html# vim config/config.php
```

Exit and restart the container.

```bash
root@f7ecbe790da1:/var/www/html# exit
exit
$ docker restart App
App
```

Execute the script to fix some warnings...

```bash
$ root@utest:/home/administrator/home-server/nextcloud# ./config.sh
```

Configure the email server using **Zoho Mail**:

![Mail Server Config](img/20.png)

#### Cron Error

To fix the cron error, first make sure **cron** is selected on the settings. Then, create a new **cronjob**.

```bash
root@server:/home/administrator# crontab -l
no crontab for root

root@server:/home/administrator# crontab -e
no crontab for root - using an empty one

Select an editor.  To change later, run 'select-editor'.
  1. /bin/nano        <---- easiest
  2. /usr/bin/vim.basic
  3. /usr/bin/vim.tiny
  4. /bin/ed

Choose 1-4 [1]: 1
```

Finally, paste this command to make sure the crontab jobs are working every **5 minutes**.

`*/5 * * * * docker exec -u www-data App php -f /var/www/html/cron.php`

### DuckDNS

It's a good practice to use a **Dynamic DNS** because the public **IP address** can change. Log in to [DuckDNS](https://www.duckdns.org/) and create a new domain pointing to the **actual IP address**.

Then, copy the `token` and paste it to the `.env` file and start the service.

Now if your **public IP address** changes, this service will update automatically.

### Domains

Once we have configured the **DynDNS**, let's create some `DNS Records` to access our services:

![DNS Records](img/21.png)

Finally, open the ports `80, 443 & 51820` on the **router** to make sure all service can work.

### Nginx Proxy Manager

If we want to access from **Internet** to some services, we have to configure the `Reverse Proxy`. Log in to the web using the [default credentials](https://nginxproxymanager.com/guide/#quick-setup). Immediately after logging in with this default user, modify your details and change your password.

Then, create some **SSL Certificates**, and following this [tutorial](https://youtu.be/qlcVx-k-02E), create one to access to our local home lab using **HTTPS**.

![SSL Certificates](img/22.png)

Create the necessary **Proxy Hosts** to their `destination`.

![Proxy Hosts](img/23.png)

Change the **default site** to `404 page`.

![Default Site](img/24.png)

To solve errors in **NextCloud**, copy and paste this in the **advanced settings** of the host.

```js
location /.well-known/carddav {
  return 301 $scheme://$host/remote.php/dav;}
location /.well-known/caldav {
  return 301 $scheme://$host/remote.php/dav; }
location /.well-known/webdav {
 return 301 $scheme://$host/remote.php/dav; }
```

### Vaultwarden

First, start the service and configure the **Reverse Proxy** to use `HTTPS`:

```bash
$ root@server:/home/administrator/server/vaultwarden# dcup
```

To [enable the admin page](https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page), generate an `Argon2id PHC` and paste the output in the `.env` file:

```bash
$ docker exec -it Vaultwarden /vaultwarden hash --preset owasp
```

Once we have the `ADMIN_TOKEN`, recreate the container:

```bash
$ root@server:/home/administrator/server/vaultwarden# dcup
```

Create a new account before edit the **Admin settings**. Navigate to the admin page and configure the **SMTP** settings:

![Mail Settings](img/25.png)

Then, [disable registration of new users](https://github.com/dani-garcia/vaultwarden/wiki/Disable-registration-of-new-users) in the general settings:

![General Settings](img/26.png)

Finally, enable the [email 2FA settings](https://bitwarden.com/help/setup-two-step-login-email/) and log in to your account and verify your **email**.

![Email 2FA settings](img/27.png)

### Wireguard

To make sure the **VPN** works always, put the `URL` of [DuckDNS](#duckdns) in the `docker-compose.yml`. Then, add the **peers** you need and start the service.

### TeamViewer QS

First, make sure to log in, using the **Xorg** session.

![Ubuntu on Xorg](img/31.png)

Download the [QuickSupport](https://www.teamviewer.com/de/download/linux/) version.

![TeamViewer web](img/32.png)

Then, navigate to the **Downloads** folder and extract the file:

```bash
$ cd Downloads
$ tar -xzf teamviewer_qs.tar.gz
```

Open the folder and execute `teamviewer`, accept the _EULA_ and continue. Then close the window.

![Accept EULA](img/33.png)

Move the folder to `/opt` and copy the `.desktop` file to the **Desktop**.

![Move to opt](img/34.png)

Make sure the file is correct:

```js
[Desktop Entry]
Version=1.0
Encoding=UTF-8
Type=Application
Categories=Network;

Name=TeamViewer QuickSupport
Comment=Remote control solution.
Exec=/opt/teamviewerqs/tv_bin/script/teamviewer

Icon=/opt/teamviewerqs/tv_bin/desktop/teamviewer_48.png
```

Finally, click on the icon and select **Allow Launching**.

![Allow Launching](img/35.png)
